package cs319;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import javax.swing.AbstractListModel;

public class DataModel extends AbstractListModel<String>{

	private ArrayList<String> list = new ArrayList<String>();

	public DataModel(File textDoc){
		try {
			BufferedReader reader = new BufferedReader(new FileReader(textDoc));
			String company = "";
			while((company = reader.readLine()) != null){
				list.add(company);
			}
		} 
		catch (FileNotFoundException e) {
			System.out.println("The file provided is not valid");
			e.printStackTrace();
		}	
		catch (IOException e) {
			System.out.println("An error was encountered while reading the file");
			e.printStackTrace();
		}
	}

	@Override
	public int getSize() {
		return list.size();
	}

	@Override
	public String getElementAt(int index) {
		return list.get(index);
	}
}
