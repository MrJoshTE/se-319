package cs319;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.io.File;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListDataListener;

import java.awt.FlowLayout;

public class Lab2Swing extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Lab2Swing frame = new Lab2Swing();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Lab2Swing() {
		setTitle("Tabbed Swing Application");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.BOTTOM);
		/*JPanel tree = new JPanel();
		tree.setName("Tree");
		tabbedPane.add(tree);*/
		tabbedPane.setBounds(0, 0, 434, 261);
		contentPane.add(tabbedPane);
		
		JPanel List = new JPanel();
		tabbedPane.addTab("List", null, List, null);
		List.setLayout(null);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(0, 0, 429, 196);
		List.add(scrollPane);
		
		DataModel CompanyData = new DataModel(new File("C:\\Users\\Josh\\workspace\\Lab2-Swing\\src\\cs319\\companies.txt"));
		DefaultListModel listModel = new DefaultListModel();
		for(int i = 0; i < CompanyData.getSize(); i++){
			listModel.addElement(CompanyData.getElementAt(i));
		}
		
		JList CompanyListPanel = new JList(listModel);
		CompanyListPanel.setBounds(0, 0, 0, 0);
		//List.add(CompanyListPanel);
		
		scrollPane.setViewportView(CompanyListPanel);
		CompanyListPanel.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		
		JButton AddButton = new JButton("Add");
		AddButton.setBounds(48, 199, 89, 23);
		List.add(AddButton);
		
		JButton RemoveButton = new JButton("Remove");
		RemoveButton.setBounds(260, 199, 89, 23);
		List.add(RemoveButton);
		
		JPanel Tree = new JPanel();
		tabbedPane.addTab("Tree", null, Tree, null);
		
		JPanel Table = new JPanel();
		tabbedPane.addTab("Table", null, Table, null);
	}
}
